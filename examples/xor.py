import jax
import jax.nn as nn
import jax.numpy as jnp

from jax_cv.utils import key_generator
from jax_cv.modules import BaseModule, Dense


def main():
    key = jax.random.PRNGKey(34)

    data_x = jnp.array([[0, 0], [0, 1], [1, 0], [1, 1]])
    data_y = jnp.array([[0], [1], [1], [0]])

    n_epochs = 100

    model = MLP(key=key, in_dim=2, hid_dim=2, out_dim=1)

    grad_fn = jax.jit(jax.grad(loss_fn))

    for epoch in range(n_epochs):
        grads = grad_fn(model, data_x, data_y)

        model = jax.tree_multimap(sgd, model, grads)

        if (epoch + 1) % (n_epochs // 10) == 0:
            loss = loss_fn(model, data_x, data_y)
            print(loss.item())
    
    y_pred = model(data_x)
    print(y_pred)


class MLP(BaseModule):
    def __init__(self, key, in_dim, hid_dim, out_dim):
        super().__init__()

        kg = key_generator(key)

        self.dense1 = Dense(next(kg), in_dim, hid_dim)
        self.dense2 = Dense(next(kg), hid_dim, out_dim)
    
    def __call__(self, x):
        x = jnp.tanh(self.dense1(x))
        return nn.sigmoid(self.dense2(x))


def loss_fn(model, x, y):
    y_pred = model(x)
    loss = - y * jnp.log(y_pred) - (1 - y) * jnp.log(1 - y_pred)
    return loss.mean()


def sgd(param, grad, lr=1.0):
    return param - lr * grad


if __name__ == '__main__':
    main()