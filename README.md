
## Installation

```shell
pip install git+https://gitlab.com/toru34/jax-cv.git
```

or 

```shell
pip clone https://gitlab.com/toru34/jax-cv
cd jax-cv
python setup.py sdist bdist_wheel
pip install ./dist/jax-cv-0.0.1.tar.gz
```