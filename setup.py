import setuptools

with open('README.md', 'r') as f:
    long_description = f.read()

setuptools.setup(
    name='jax-cv',
    version='0.0.1',
    author='Toru Fujino',
    author_email='toru.fb34@gmail.com',
    description='JAX\'s neural network modules and models',
    long_description=long_description,
    url='https://gitlab.com/toru34/jax-cv',
    packages=setuptools.find_packages(),
    python_requires='>=3.6'
)
