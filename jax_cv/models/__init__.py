from jax_cv.models.resnet.resnet18 import ResNet18
from jax_cv.models.resnet.resnet34 import ResNet34
from jax_cv.models.resnet.resnet50 import ResNet50
from jax_cv.models.resnet.resnet101 import ResNet101
from jax_cv.models.resnet.resnet152 import ResNet152
# from jax_cv.models.vgg.vgg16 import VGG16
# from jax_cv.models.vgg.vgg19 import VGG19