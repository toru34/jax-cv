import jax.nn as nn

from jax_cv.utils import key_generator
from jax_cv.modules import BaseModule, Conv


class RegionProposalNetwork(BaseModule):
    def __init__(self, key, in_filters, anchor_scales=(128, 256, 512), anchor_aspect_ratios=(0.5, 1.0, 2.0)):
        super().__init__()

        kg = key_generator(key)

        n_anchors = len(anchor_scales) * len(anchor_aspect_ratios)

        self.conv = Conv(next(kg), (in_filters, in_filters, 3, 3))
        self.conv_objectness = Conv(next(kg), (n_anchors, in_filters, 1, 1))
        self.conv_regression = Conv(next(kg), (4 * n_anchors, in_filters, 1, 1))
    

    def __call__(self, x):
        objectness, regression = [], []

        for feature in x:
            feature = nn.relu(self.conv(feature))

            objectness.append(self.conv_objectness(feature))
            regression.append(self.conv_regression(feature))

        return objectness, regression