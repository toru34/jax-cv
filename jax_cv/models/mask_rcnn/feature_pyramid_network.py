from collections import OrderedDict

import jax

from jax_cv.utils import key_generator
from jax_cv.modules import BaseModule, Conv

backbones_in_filters_list = {
    'resnet50': [64, 256, 512, 1024, 2048],
}


class FeaturePyramidNetwork(BaseModule):
    def __init__(self, key, backbone='resnet50', out_filters=256):
        super().__init__()

        kg = key_generator(key)

        in_filters_list = backbones_in_filters_list[backbone]

        self.conv_laterals = []
        self.conv_reducealiases = []

        for feature_level, in_filters in enumerate(in_filters_list):
            self.conv_laterals.append(Conv(next(kg), (out_filters, in_filters, 1, 1))) # TODO
            self.conv_reducealiases.append(Conv(next(kg), (out_filters, out_filters, 3, 3)))
        
        self.n_feature_levels = len(in_filters_list)


    def __call__(self, x):
        features = []
        for i, feature in enumerate(x[::-1]):
            feature_level = self.n_feature_levels - i - 1

            if feature_level == self.n_feature_levels - 1:
                feature = self.conv_laterals[feature_level](feature)
            else:
                feature = self.conv_laterals[feature_level](feature) + self._upsample_feature(feature_upper)
            
            features.append(feature)
            
            feature_upper = feature

        return features
    

    @staticmethod
    def _upsample_feature(x):
        n, c, h, w = x.shape
        return jax.image.resize(x, (n, c, 2 * h, 2 * w), method='nearest')

            