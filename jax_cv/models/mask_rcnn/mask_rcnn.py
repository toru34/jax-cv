from jax_cv.utils import key_generator
from jax_cv.models import ResNet50
from jax_cv.modules import BaseModule
from jax_cv.models.mask_rcnn.feature_pyramid_network import FeaturePyramidNetwork
from jax_cv.models.mask_rcnn.region_proposal_network import RegionProposalNetwork

class MaskRCNN(BaseModule):
    def __init__(self, key, backbone='resnet50'):

        kg = key_generator(key)

        self.backbone = ResNet50(next(kg), as_backbone=True) # TODO
        self.fpn = FeaturePyramidNetwork(next(kg), backbone=backbone, out_filters=256)
        self.rpn = RegionProposalNetwork(next(kg), in_filters=256)


    def __call__(self, x):
        features = self.backbone(x)
        features = self.fpn(features)
        objectness, regression = self.rpn(features)

        return objectness, regression