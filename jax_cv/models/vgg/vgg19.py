import os
import pickle

import jax
import jax.nn as nn

from ..base import Module
from ..modules import Dense, Conv, MaxPool

class VGG19(Module):
    def __init__(self, key, use_pretrained_weights=False):
        super().__init__()
        
        keys = jax.random.split(key, num=19)
        
        self.block1_conv1 = Conv(keys[0], (64, 3, 3, 3))
        self.block1_conv2 = Conv(keys[1], (64, 64, 3, 3))
        self.block1_pool  = MaxPool()

        self.block2_conv1 = Conv(keys[2], (128, 64, 3, 3))
        self.block2_conv2 = Conv(keys[3], (128, 128, 3, 3))
        self.block2_pool  = MaxPool()

        self.block3_conv1 = Conv(keys[4], (256, 128, 3, 3))
        self.block3_conv2 = Conv(keys[5], (256, 256, 3, 3))
        self.block3_conv3 = Conv(keys[6], (256, 256, 3, 3))
        self.block3_conv4 = Conv(keys[7], (256, 256, 3, 3))
        self.block3_pool  = MaxPool()

        self.block4_conv1 = Conv(keys[8], (512, 256, 3, 3))
        self.block4_conv2 = Conv(keys[9], (512, 512, 3, 3))
        self.block4_conv3 = Conv(keys[10], (512, 512, 3, 3))
        self.block4_conv4 = Conv(keys[11], (512, 512, 3, 3))
        self.block4_pool  = MaxPool()

        self.block5_conv1 = Conv(keys[12], (512, 512, 3, 3))
        self.block5_conv2 = Conv(keys[13], (512, 512, 3, 3))
        self.block5_conv3 = Conv(keys[14], (512, 512, 3, 3))
        self.block5_conv4 = Conv(keys[15], (512, 512, 3, 3))
        self.block5_pool  = MaxPool()
        
        self.block6_dense1 = Dense(keys[16], 512 * 7 * 7, 4096)
        self.block6_dense2 = Dense(keys[17], 4096, 4096)
        self.block6_dense3 = Dense(keys[18], 4096, 1000)

        if use_pretrained_weights:
            self._load_pretrained_weights()
    
    def __call__(self, x):
        x = nn.relu(self.block1_conv1(x))
        x = nn.relu(self.block1_conv2(x))
        x = self.block1_pool(x)
        
        x = nn.relu(self.block2_conv1(x))
        x = nn.relu(self.block2_conv2(x))
        x = self.block2_pool(x)

        x = nn.relu(self.block3_conv1(x))
        x = nn.relu(self.block3_conv2(x))
        x = nn.relu(self.block3_conv3(x))
        x = nn.relu(self.block3_conv4(x))
        x = self.block3_pool(x)

        x = nn.relu(self.block4_conv1(x))
        x = nn.relu(self.block4_conv2(x))
        x = nn.relu(self.block4_conv3(x))
        x = nn.relu(self.block4_conv4(x))
        x = self.block4_pool(x)

        x = nn.relu(self.block5_conv1(x))
        x = nn.relu(self.block5_conv2(x))
        x = nn.relu(self.block5_conv3(x))
        x = nn.relu(self.block5_conv4(x))
        x = self.block5_pool(x)
        
        x = x.reshape(x.shape[0], -1)
        
        x = nn.relu(self.block6_dense1(x))
        x = nn.relu(self.block6_dense2(x))
        x = self.block6_dense3(x)
        return x

    def _load_pretrained_weights(self):
        # base_url = ''
        file_name = 'vgg19_weights.pickle'

        pretrained_weights_dir = os.path.join(os.path.expanduser('~'), '.jax_nn/pretrained_weights/')

        # Create directory if it doesn't exist
        if not os.path.exists(pretrained_weights_dir):
            os.makedirs(pretrained_weights_dir)

        # Download pretrained weights if they don't exist
        if not os.path.isfile(os.path.join(pretrained_weights_dir, file_name)):
            raise NotImplementedError
        
        # Read pretrained weights
        with open(os.path.join(pretrained_weights_dir, file_name), 'rb') as f:
            pretrained_weights = pickle.load(f)
        
        # Set pretrained weights
        self.set_weights(pretrained_weights)