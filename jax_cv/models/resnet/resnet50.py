from collections import OrderedDict

import jax
import jax.nn as nn

from jax_cv.utils import key_generator
from jax_cv.modules import BaseModule, Dense, Conv, MaxPool, BatchNorm, GlobalAveragePool, Sequential
from jax_cv.models.resnet.resblock import ResBlock


# ImageNet: https://u.pcloud.link/publink/show?code=XZlhLvkZ4Hv5dhzruA7JrhnHBYvO5foPm6sy
class ResNet50(BaseModule):
    def __init__(
        self, key, pretrained_weights=None, as_backbone=False):
        super().__init__()

        kg = key_generator(key)

        self.as_backbone = as_backbone

        self.block1 = Sequential(OrderedDict({
            'conv1': Conv(next(kg), (64, 3, 7, 7), stride=(2, 2)),
            'batch_norm1': BatchNorm(64),
            'relu1': nn.relu
        }))

        self.block2 = Sequential(OrderedDict({
            'pool1': MaxPool((3, 3), stride=(2, 2)),
            'resblock1': ResBlock(next(kg), (
                (64, 64, 1, 1),
                (64, 64, 3, 3),
                (256, 64, 1, 1)
            )),
            'resblock2': ResBlock(next(kg), (
                (64, 256, 1, 1),
                (64, 64, 3, 3),
                (256, 64, 1, 1)
            )),
            'resblock3': ResBlock(next(kg), (
                (64, 256, 1, 1),
                (64, 64, 3, 3),
                (256, 64, 1, 1)
            ))
        }))

        self.block3 = Sequential(OrderedDict({
            f'resblock{i+1}': ResBlock(next(kg), (
                (128, 256, 1, 1),
                (128, 128, 3, 3),
                (512, 128, 1, 1)
            ), halve_resolution=True)
            if i == 0 else ResBlock(next(kg), (
                (128, 512, 1, 1),
                (128, 128, 3, 3),
                (512, 128, 1, 1)
            )) for i in range(4)
        }))

        self.block4 = Sequential(OrderedDict({
            f'resblock{i+1}': ResBlock(next(kg), (
                (256, 512, 1, 1),
                (256, 256, 3, 3),
                (1024, 256, 1, 1)
            ), halve_resolution=True)
            if i == 0 else ResBlock(next(kg), (
                (256, 1024, 1, 1),
                (256, 256, 3, 3),
                (1024, 256, 1, 1)
            )) for i in range(6)
        }))

        self.block5 = Sequential(OrderedDict({
            f'resblock{i+1}': ResBlock(next(kg), (
                (512, 1024, 1, 1),
                (512, 512, 3, 3),
                (2048, 512, 1, 1)
            ), halve_resolution=True)
            if i == 0 else ResBlock(next(kg), (
                (512, 2048, 1, 1),
                (512, 512, 3, 3),
                (2048, 512, 1, 1)
            )) for i in range(3)
        }))

        if not self.as_backbone:
            self.block6 = Sequential(OrderedDict({
                'pool': GlobalAveragePool(),
                'dense': Dense(next(kg), 2048, 1000)
            }))

        if pretrained_weights is not None:
            raise NotImplementedError


    def __call__(self, x):
        if not self.as_backbone:
            x = self.block1(x) # (N, 64, 112, 112)
            x = self.block2(x) # (N, 256, 56, 56)
            x = self.block3(x) # (N, 512, 28, 28)
            x = self.block4(x) # (N, 1024, 14, 14)
            x = self.block5(x) # (N, 2048, 7, 7)
            x = self.block6(x) # (N, 1000)

            return x
        else:
            features = []

            x = self.block1(x)
            features.append(x)

            x = self.block2(x)
            features.append(x)

            x = self.block3(x)
            features.append(x)

            x = self.block4(x)
            features.append(x)

            x = self.block5(x)
            features.append(x)

            return features