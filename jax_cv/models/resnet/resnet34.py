from collections import OrderedDict

import jax
import jax.nn as nn

from jax_cv.utils import key_generator
from jax_cv.modules import BaseModule
from jax_cv.modules import Dense, Conv, MaxPool, BatchNorm, GlobalAveragePool, Sequential
from jax_cv.models.resnet.resblock import ResBlock


class ResNet34(BaseModule):
    def __init__(self, key, pretrained_weights=None):
        super().__init__()

        _kg = key_generator(key)

        self.block1 = Sequential(OrderedDict({
            'conv1': Conv(next(_kg), (64, 3, 7, 7), stride=(2, 2)),
            'bn1': BatchNorm(64),
            'relu1': nn.relu
        }))

        self.block2 = Sequential(OrderedDict({
            'pool1': MaxPool((3, 3), stride=(2, 2)),
            'resblock1': ResBlock(next(_kg), (
                (64, 64, 3, 3),
                (64, 64, 3, 3)
            )),
            'resblock2': ResBlock(next(_kg), (
                (64, 64, 3, 3),
                (64, 64, 3, 3)
            )),
            'resblock3': ResBlock(next(_kg), (
                (64, 64, 3, 3),
                (64, 64, 3, 3)
            ))
        }))

        self.block3 = Sequential(OrderedDict({
            f'resblock{i+1}': ResBlock(next(_kg), (
                (128, 64, 3, 3),
                (128, 128, 3, 3)
            ), halve_resolution=True)
            if i == 0 else ResBlock(next(_kg), (
                (128, 128, 3, 3),
                (128, 128, 3, 3)
            )) for i in range(4)
        }))

        self.block4 = Sequential(OrderedDict({
            f'resblock{i+1}': ResBlock(next(_kg), (
                (256, 128, 3, 3),
                (256, 256, 3, 3)
            ), halve_resolution=True)
            if i == 0 else ResBlock(next(_kg), (
                (256, 256, 3, 3),
                (256, 256, 3, 3)
            )) for i in range(6)
        }))

        self.block5 = Sequential(OrderedDict({
            f'resblock{i+1}': ResBlock(next(_kg), (
                (512, 256, 3, 3),
                (512, 512, 3, 3)
            ), halve_resolution=True)
            if i == 0 else ResBlock(next(_kg), (
                (512, 512, 3, 3),
                (512, 512, 3, 3)
            )) for i in range(6)
        }))

        self.block6 = Sequential(OrderedDict({
            'pool': GlobalAveragePool(),
            'dense': Dense(next(_kg), 512, 1000)
        }))

        if pretrained_weights is not None:
            raise NotImplementedError


    def __call__(self, x):
        x = self.block1(x)
        x = self.block2(x)
        x = self.block3(x)
        x = self.block4(x)
        x = self.block5(x)
        x = self.block6(x)

        return x