from collections import OrderedDict

import jax
import jax.nn as nn

from jax_cv.utils import key_generator
from jax_cv.modules import BaseModule
from jax_cv.modules import Dense, Conv, MaxPool, BatchNorm, GlobalAveragePool, Sequential
from jax_cv.models.resnet.resblock import ResBlock


class ResNet152(BaseModule):
    def __init__(self, random_key, pretrained_weights=None):
        super().__init__()

        _kg = key_generator(random_key)

        self.block1 = Sequential(OrderedDict({
            'conv1': Conv(next(_kg), (64, 3, 7, 7), stride=(2, 2)),
            'bn1': BatchNorm(64),
            'relu1': nn.relu
        }))

        self.block2 = Sequential(OrderedDict({
            'pool1': MaxPool((3, 3), stride=(2, 2)),
            'resblock1': ResBlock(next(_kg), (
                (64, 64, 1, 1),
                (64, 64, 3, 3),
                (256, 64, 1, 1)
            )),
            'resblock2': ResBlock(next(_kg), (
                (64, 256, 1, 1),
                (64, 64, 3, 3),
                (256, 64, 1, 1)
            )),
            'resblock3': ResBlock(next(_kg), (
                (64, 256, 1, 1),
                (64, 64, 3, 3),
                (256, 64, 1, 1)
            ))
        }))

        self.block3 = Sequential(OrderedDict({
            f'resblock{i+1}': ResBlock(next(_kg), (
                (128, 256, 1, 1),
                (128, 128, 3, 3),
                (512, 128, 1, 1)
            ), halve_resolution=True)
            if i == 0 else ResBlock(next(_kg), (
                (128, 512, 1, 1),
                (128, 128, 3, 3),
                (512, 128, 1, 1)
            )) for i in range(8)
        }))

        self.block4 = Sequential(OrderedDict({
            f'resblock{i+1}': ResBlock(next(_kg), (
                (256, 512, 1, 1),
                (256, 256, 3, 3),
                (1024, 256, 1, 1)
            ), halve_resolution=True)
            if i == 0 else ResBlock(next(_kg), (
                (256, 1024, 1, 1),
                (256, 256, 3, 3),
                (1024, 256, 1, 1)
            )) for i in range(36)
        }))

        self.block5 = Sequential(OrderedDict({
            f'resblock{i+1}': ResBlock(next(_kg), (
                (512, 1024, 1, 1),
                (512, 512, 3, 3),
                (2048, 512, 1, 1)
            ), halve_resolution=True)
            if i == 0 else ResBlock(next(_kg), (
                (512, 2048, 1, 1),
                (512, 512, 3, 3),
                (2048, 512, 1, 1)
            )) for i in range(3)
        }))

        self.block6 = Sequential(OrderedDict({
            'pool': GlobalAveragePool(),
            'dense': Dense(next(_kg), 2048, 1000)
        }))

        if pretrained_weights is not None:
            raise NotImplementedError


    def __call__(self, x):
        x = self.block1(x)
        x = self.block2(x)
        x = self.block3(x)
        x = self.block4(x)
        x = self.block5(x)
        x = self.block6(x)

        return x