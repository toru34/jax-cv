import jax
import jax.nn as nn

from jax_cv.utils import key_generator
from jax_cv.modules import BaseModule, Conv, BatchNorm


class ResBlock(BaseModule):
    def __init__(self, key, filter_shape_list, halve_resolution=False):
        super().__init__()

        kg = key_generator(key)

        self.in_filters = filter_shape_list[0][1]
        self.out_filters = filter_shape_list[-1][0]
        self.n_convs = len(filter_shape_list)

        self.halve_resolution = halve_resolution

        if self.in_filters != self.out_filters or halve_resolution:
            self.conv0 = Conv(
                key=next(kg),
                filter_shape=(self.out_filters, self.in_filters, 1, 1),
                stride=(2, 2) if halve_resolution else (1, 1),
                use_bias=False
            )
            self.batch_norm0 = BatchNorm(
                n_filters=self.out_filters
            )

        for i, filter_shape in enumerate(filter_shape_list):
            setattr(self, f'conv{i+1}', Conv(
                key=next(kg),
                filter_shape=filter_shape,
                stride=(2, 2) if halve_resolution and i == 0 else (1, 1),
                use_bias=False
            ))
            setattr(self, f'batch_norm{i+1}', BatchNorm(
                n_filters=filter_shape[0]
            ))


    def __call__(self, x):
        residual = x

        if self.in_filters != self.out_filters or self.halve_resolution:
            residual = self.conv0(residual)
            residual = self.batch_norm0(residual)

        for i in range(self.n_convs):
            x = getattr(self, f'conv{i+1}')(x)
            x = getattr(self, f'batch_norm{i+1}')(x)

            if i == self.n_convs - 1:
                x += residual
            
            x = nn.relu(x)

        return x