import numpy as np
import jax
import jax.numpy as jnp
from PIL import Image, ImageDraw
from skimage import measure # TODO: avoid using skimage


def key_generator(key):
    while True:
        key, key_new = jax.random.split(key)
        yield key_new


# TODO: np? or jnp?
def _rle_to_mask(rle, shape):
    mask = np.concatenate([np.zeros(length, dtype='uint8') if i % 2 == 0 else np.ones(length, dtype='uint8') for i, length in enumerate(rle)]).reshape(shape[::-1]).T

    return mask


# TODO: np? or jnp?
def _polygons_to_mask(polygons, shape=None):
    """
    """
    # TODO:
    if shape is None:
        width, height = np.concatenate([np.array(polygon).reshape(-1, 2) for polygon in polygons]).max(axis=0).astype('int32').tolist()
    else:
        width, height = shape

    mask = Image.fromarray(np.zeros((height, width), dtype='uint8'))
    
    draw = ImageDraw.Draw(mask)
    for polygon in polygons:
        draw.polygon(polygon, fill=1, outline=1)
    
    mask = np.array(mask).astype('uint8')

    return mask


def _mask_to_polygons(mask):
    polygons = [contour[:, ::-1].flatten().tolist() for contour in measure.find_contours(mask, 0.5)]

    return polygons


def _rle_to_polygons(rle, shape):
    # TODO:
    mask = _rle_to_mask(rle, shape)
    polygons = _mask_to_polygons(mask)

    return polygons