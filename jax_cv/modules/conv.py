import math

import jax
import jax.numpy as jnp
from jaxnm import BaseModule

from jax_cv.utils import key_generator
from jax_cv.modules.utils import _pad


class Conv2d(BaseModule):
    def __init__(self, key, in_channels, out_channels, kernel_size, stride=1, padding='SAME', use_bias=True):
        super().__init__()

        self.W = jax.random.normal(key, (out_channels, in_channels, kernel_size, kernel_size))

        self.use_bias = use_bias
        if self.use_bias:
            self.b = jnp.zeros(out_channels)
        
        self.stride  = stride
        self.padding = padding
        if self.padding == 'SAME':
            self.padding_width = math.floor(kernel_size / 2)
        elif self.padding == 'VALID':
            self.padding_width = 0
        else:
            raise NotImplementedError


    def forward(self, x):
        x = _pad(x, self.padding_width)
        x = jax.lax.conv(x, self.W, window_strides=(self.stride, self.stride), padding='VALID') # TODO:

        if self.use_bias:
            return x + self.b[None, :, None, None]
        else:
            return x


class GroupConv2d(BaseModule):
    def __init__(self, key, in_channels, out_channels, kernel_size, n_groups, stride=1, padding='SAME', use_bias=True):
        super().__init__()

        assert in_channels % n_groups == 0
        assert out_channels % n_groups == 0

        in_channels_per_group = in_channels // n_groups
        out_channels_per_group = out_channels // n_groups

        self.n_groups = n_groups
        self.in_channels_per_group = in_channels_per_group

        kg = key_generator(key)

        for i in range(self.n_groups):
            setattr(self, f'conv{i+1}', Conv(next(kg), in_channels_per_group, out_channels_per_group, kernel_size, stride, padding, use_bias))
        
        self.use_bias = use_bias
        if self.use_bias:
            self.b = jnp.zeros(out_channels)
    
    def forward(self, x):
        features = []
        for i in range(self.n_groups):
            start = i * self.in_channels_per_group
            end = (i + 1) * self.in_channels_per_group

            features.append(getattr(self, f'conv{i+1}')(x[:, start:end]))
            
        features = jnp.concatenate(features, axis=1)

        if self.use_bias:
            return features + self.b[None, :, None, None]
        else:
            return features