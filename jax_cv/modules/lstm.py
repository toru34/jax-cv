import jax
import jax.nn as nn
import jax.numpy as jnp

from jaxnm import BaseModule


class LSTMCell(BaseModule):
    def __init__(self, key, in_dim, hid_dim):
        super().__init__()
        
        self.W = jax.random.normal(key, (in_dim + hid_dim, 4 * hid_dim))
        self.b = np.zeros(4 * hid_dim)


    def __call__(self, x_t, prev_states): # TODO: add mask
        c_tm1, h_tm1 = prev_states
        
        xh_t = np.concatenate([x_t, h_tm1], axis=1)
        
        _i_t, _f_t, _o_t, _c_t = np.split(xh_t @ self.W + self.b, indices_or_sections=4, axis=1)
        
        i_t = nn.sigmoid(_i_t)
        f_t = nn.sigmoid(_f_t)
        o_t = nn.sigmoid(_o_t)
        c_t = f_t * c_tm1 + i_t * np.tanh(_c_t)
        
        h_t = o_t * np.tanh(c_t)
        
        return c_t, h_t


class LSTM(BaseModule):
    def __init__(self, key, in_dim, hid_dim):
        super().__init__()
        
        self.cell = LSTMCell(key, in_dim, hid_dim)
        
        self.hid_dim = hid_dim


    def __call__(self, x):
        # (N, S, E) -> (S, N, E)
        x = x.transpose((1, 0, 2))

        c_tm1 = np.zeros((x.shape[1], self.hid_dim)) # (N, H)
        h_tm1 = np.tanh(c_tm1)
        
        h = []
        for x_t in x:
            c_t, h_t = self.cell(x_t, (c_tm1, h_tm1))
            
            h.append(h_t)
            
            c_tm1 = c_t
            h_tm1 = h_t
        
        h = np.stack(h, axis=1) # (N, S, H)
        
        return h