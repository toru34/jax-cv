from jaxnm import BaseModule


class Sequential(BaseModule):
    def __init__(self, module_ordereddict):
        super().__init__()

        for name, module in module_ordereddict.items():
            setattr(self, name, module)
        
        self._names = list(module_ordereddict.keys())


    def __call__(self, x):
        for _name in self._names:
            x = getattr(self, _name)(x)
        
        return x