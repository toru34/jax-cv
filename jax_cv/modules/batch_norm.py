import jax.numpy as jnp
from jaxnm import BaseModule


class BatchNorm(BaseModule):
    def __init__(self, n_filters):
        super().__init__()

        self.gamma = jnp.ones(n_filters, dtype=jnp.float32) # TODO:
        self.beta = jnp.zeros(n_filters, dtype=jnp.float32) # TODO:

        self.epsilon = 1e-7 # TODO: jax-cv全体の定数としてどこかで指定する
        self.momentum = 0.1

        self.population_stats = {
            'mean': jnp.zeros(n_filters, dtype=jnp.float32), # TODO:
            'var': jnp.ones(n_filters, dtype=jnp.float32)} # TODO:


    def __call__(self, x):
        if self._eval:
            x_mean = self.population_stats['mean']
            x_var = self.population_stats['var']
        else:
            x_mean = jnp.mean(x, axis=(0, 2, 3))
            x_var = jnp.var(x, axis=(0, 2, 3))

            # Update population statistics
            self.population_stats['mean'] = self.momentum * x_mean + (1 - self.momentum) * self.population_stats['mean']
            self.population_stats['var'] = self.momentum * x_var + (1 - self.momentum) * self.population_stats['var']
        
        x_normalised = (x - x_mean[None, :, None, None]) / jnp.sqrt(x_var[None, :, None, None] + self.epsilon) # TODO: replace epsilon with something global
        
        return self.gamma[None, :, None, None] * x_normalised + self.beta[None, :, None, None] # TODO: