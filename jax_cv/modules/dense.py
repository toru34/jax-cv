import jax
import jax.numpy as jnp

from jaxnm import BaseModule


class Dense(BaseModule):
    def __init__(self, key, in_dim, out_dim):
        super().__init__()
        
        self.W = jax.random.normal(key, (in_dim, out_dim))
        self.b = jnp.zeros(out_dim)


    def __call__(self, x):
        return x @ self.W + self.b