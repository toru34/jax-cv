import math

import jax
import jax.numpy as jnp

from jaxnm import BaseModule

from jax_cv.modules.utils import _pad


class MaxPool(BaseModule):
    def __init__(self, kernel_size=2, stride=2, padding='SAME'):
        super().__init__()

        self.kernel_size = kernel_size
        self.stride = stride
        self.padding = padding

        if self.padding == 'SAME':
            self.padding_width = math.floor(kernel_size / 2)
        elif self.padding == 'VALID':
            self.padding_width = 0
        else:
            raise NotImplementedError

    def forward(self, x):
        x = _pad(x, self.padding_width)
        return jax.lax.reduce_window(x, -jnp.inf, jax.lax.max, window_dimensions=(1, 1, self.kernel_size, self.kernel_size), window_strides=(1, 1, self.stride, self.stride), padding='VALID')


class GlobalAveragePool(BaseModule):
    def __init__(self):
        super().__init__()

    def forward(self, x):
        return jnp.mean(x, axis=(2, 3))