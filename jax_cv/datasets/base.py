class BaseDataset:
    def __init__(self):
        pass


    def __getitem__(self, i):
        raise NotImplementedError


    def __len__(self):
        raise NotImplementedError


    @staticmethod
    def _download_dataset():
        raise NotImplementedError
