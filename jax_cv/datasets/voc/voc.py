import xml.etree.ElementTree as ET
from pathlib import Path

import matplotlib.image as mpimage

from jax_cv.datasets.utils import _download_file


dataset_dir = Path.home() / '.jax_cv/datasets/voc2012'
image_dir = dataset_dir / 'VOCtrainval/VOC2012/JPEGImages'
annotation_dir = dataset_dir / 'VOCtrainval/VOC2012/Annotations'
dataset_split_dir = dataset_dir / 'VOCtrainval/VOC2012/ImageSets/Main'


class VOCDataset:
    def __init__(self, set_type='train'):
        """
        """
        self.images = []
        self.annotations = []
        self.categories = []

        # Download dataset if it doesn't exist
        if not dataset_dir.is_dir():
            self._download_dataset()
        
        if set_type == 'train':
            ids = [line.strip() for line in open(dataset_split_dir / 'train.txt', 'r').readlines()]
        elif set_type == 'valid':
            ids = [line.strip() for line in open(dataset_split_dir / 'val.txt', 'r').readlines()]
        elif set_type == 'test':
            raise NotImplementedError
        else:
            raise NotImplementedError
        
        for image_path, annotation_path in zip(sorted(image_dir.glob('*.jpg')), sorted(annotation_dir.glob('*.xml'))):
            assert image_path.stem == annotation_path.stem

            if image_path.stem not in ids:
                continue
    
            tree = ET.parse(annotation_path)
            root = tree.getroot()

            image_info = {
                'path': image_path,
                'height': int(root.find('size').find('height').text),
                'width': int(root.find('size').find('width').text)
            }

            # Add instance information
            annotation_info = []
            for _instance in root.findall('object'):
                _bbox = _instance.find('bndbox')
                _category = _instance.find('name').text
                
                instance = {
                    'category': _category,
                    'bbox': {
                        'xmin': int(_bbox.find('xmin').text),
                        'ymin': int(_bbox.find('ymin').text),
                        'xmax': int(_bbox.find('xmax').text),
                        'ymax': int(_bbox.find('ymax').text)
                    }
                }

                annotation_info.append(instance)

                if _category not in self.categories:
                    self.categories.append(_category)
            
            self.images.append(image_info)
            self.annotations.append(annotation_info)


    def __getitem__(self, i):
        image = mpimage.imread(self.images[i]['path'])
        annotation = self.annotations[i]

        return image, annotation


    def __len__(self):
        return len(self.images)


    @staticmethod
    def _download_dataset():
        # TODO:
        pass