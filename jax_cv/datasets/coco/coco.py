import json
from pathlib import Path
from collections import defaultdict

import numpy as np
import matplotlib.image as mpimage
from PIL import Image, ImageDraw

from jax_cv.datasets.utils import _download_file


_dataset_dir = Path.home() / '.jax_cv/datasets/coco2017'

_train_image_dir = _dataset_dir / 'train2017'
_valid_image_dir = _dataset_dir / 'val2017'

_train_annotation_path = _dataset_dir / 'annotations/instances_train2017.json'
_valid_annotation_path = _dataset_dir / 'annotations/instances_val2017.json'


# TODO: inherit BaseDataset class
class COCODataset:
    def __init__(self, set_type='train'):
        """
        """
        self.images = []
        self.annotations = []

        # Download dataset if it doesn't exist
        if not _dataset_dir.is_dir():
            self._download_dataset()

        if set_type == 'train':
            _image_dir = _train_image_dir
            _annotation_path = _train_annotation_path
        elif set_type == 'valid':
            _image_dir = _valid_image_dir
            _annotation_path = _valid_annotation_path
        elif set_type == 'test':
            raise NotImplementedError
        else:
            raise NotImplementedError
        
        dataset_info = json.load(open(_annotation_path, 'r'))

        self.category2id = {c['name']: i for i, c in enumerate(dataset_info['categories'])}
        self.id2category = {i: category for category, i in self.category2id.items()}

        _id2image = dict()
        _id2annotation = defaultdict(list)

        _catid2category = {c['id']: c['name'] for c in dataset_info['categories']}

        for _image_info in dataset_info['images']:
            _id = _image_info['id']

            image_info = {
                'path': _image_dir / _image_info['file_name'],
                'height': _image_info['height'],
                'width': _image_info['width']
            }

            _id2image[_image_info['id']] = image_info
        
        for _instance in dataset_info['annotations']:
            _id = _instance['image_id']

            if _instance['iscrowd']:
                # TODO
                continue
            
            if _id not in _id2image:
                continue
            
            category = _catid2category[_instance['category_id']]
            
            instance = {
                'category': category,
                'category_id': self.category2id[category],
                'polygons': _instance['segmentation'],
                'bbox': {
                    'xmin': _instance['bbox'][0],
                    'ymin': _instance['bbox'][1],
                    'xmax': _instance['bbox'][0] + _instance['bbox'][2],
                    'ymax': _instance['bbox'][1] + _instance['bbox'][3]
                }
            }

            _id2annotation[_id].append(instance)
        
        for _id in _id2image.keys():
            if _id in _id2annotation:
                self.images.append(_id2image[_id])
                self.annotations.append(_id2annotation[_id])


    def __getitem__(self, i):
        image = mpimage.imread(self.images[i]['path'])
        annotation = self.annotations[i]

        for j in range(len(annotation)):
            mask = Image.fromarray(np.zeros(image.shape[:2], dtype='uint8'))
            draw = ImageDraw.Draw(mask)

            for polygon in annotation[j]['polygons']:
                draw.polygon(polygon, fill=1, outline=1)
            
            annotation[j]['mask'] = np.array(mask, dtype='uint8')
        
        return image, annotation
    

    def __len__(self):
        return len(self.images)


    @staticmethod
    def _download_dataset():
        # TODO
        raise NotImplementedError


# base_url = 'http://images.cocodataset.org/'
# dataset_dir = os.path.join(base_dir, 'coco2014')

# annotation_file = 'annotations_trainval2014.zip'
# test_image_info_file = 'image_info_test2014.zip'
# train_image_file = 'train2014.zip'
# valid_image_file = 'val2014.zip'
# test_image_file = 'test2014.zip'

# # Create directory if it doesn't exist
# if not os.path.exists(dataset_dir):
#     os.makedirs(dataset_dir)

# # Download dataset if it doesn't exist
# for file_name in [annotation_file, test_image_info_file]:
#     if not os.path.isfile(os.path.join(dataset_dir, file_name)):
#         _download_file(f'{base_url}annotations/', file_name, dataset_dir)

# for file_name in [train_image_file, valid_image_file, test_image_file]:
#     if not os.path.isfile(os.path.join(dataset_dir, file_name)):
#         _download_file(f'{base_url}zips/', file_name, dataset_dir)