import xml.etree.ElementTree as ET
from pathlib import Path

import matplotlib.image as mpimage

from jax_cv.datasets.utils import _download_file


_dataset_dir = Path.home() / '.jax_cv' / 'datasets' / 'bccd'
_image_dir = _dataset_dir / 'BCCD_Dataset-master' / 'BCCD' / 'JPEGImages'
_annotation_dir = _dataset_dir / 'BCCD_Dataset-master' / 'BCCD' / 'Annotations'
_dataset_split_dir = _dataset_dir / 'BCCD_Dataset-master' / 'BCCD' / 'ImageSets' / 'Main'


class BCCDDataset:
    def __init__(self, set_type='train'):
        """
        """
        self.images = []
        self.annotations = []
        self.categories = []

        # Download dataset if it doesn't exist
        if not _dataset_dir.is_dir():
            self._download_dataset()
        
        if set_type == 'train':
            _ids = [line.strip() for line in open(_dataset_split_dir / 'train.txt', 'r').readlines()]
        elif set_type == 'valid':
            _ids = [line.strip() for line in open(_dataset_split_dir / 'val.txt', 'r').readlines()]
        elif set_type == 'test':
            _ids = [line.strip() for line in open(_dataset_split_dir / 'test.txt', 'r').readlines()]
        else:
            raise NotImplementedError

        for image_path, annotation_path in zip(sorted(_image_dir.glob('*.jpg')), sorted(_annotation_dir.glob('*.xml'))):
            assert image_path.stem == annotation_path.stem

            if image_path.stem not in _ids:
                continue

            tree = ET.parse(annotation_path)
            root = tree.getroot()

            image_info = {
                'path': image_path,
                'height': int(root.find('size').find('height').text),
                'width': int(root.find('size').find('width').text)
            }

            # Add instance information
            annotation_info = []
            for _instance in root.findall('object'):
                _bbox = _instance.find('bndbox')
                _category = _instance.find('name').text
                instance = {
                    'category': _category,
                    'bbox': {
                        'xmin': int(_bbox.find('xmin').text),
                        'ymin': int(_bbox.find('ymin').text),
                        'xmax': int(_bbox.find('xmax').text),
                        'ymax': int(_bbox.find('ymax').text)
                    }
                }

                annotation_info.append(instance)

                if _category not in self.categories:
                    self.categories.append(_category)
            
            self.images.append(image_info)
            self.annotations.append(annotation_info)


    def __getitem__(self, i):
        image = mpimage.imread(self.images[i]['path'])
        annotation = self.annotations[i]

        return image, annotation


    def __len__(self):
        return len(self.images)


    @staticmethod
    def _download_dataset():
        _download_file(
            base_url='https://github.com/Shenggan/BCCD_Dataset/archive/',
            file_name='master.zip',
            save_dir=_dataset_dir,
            unpack=True
        )