import os
import zipfile
import requests
from tqdm import tqdm


def _download_file(base_url, file_name, save_dir, unpack=False):
    if not save_dir.is_dir():
        save_dir.mkdir(parents=True)

    r = requests.get(f'{base_url}{file_name}', stream=True)

    total_file_size = int(r.headers.get('content-length', 0))
    block_file_size = 1024
    
    print(f'Downloading {file_name} from {base_url}{file_name}')
    
    t = tqdm(total=total_file_size, unit='iB', unit_scale=True)
    with open(os.path.join(save_dir, file_name), 'wb') as f:
        for data in r.iter_content(block_file_size):
            t.update(len(data))
            f.write(data)
    
    t.close()

    if unpack:
        with zipfile.ZipFile(os.path.join(save_dir, file_name), 'r') as f:
            f.extractall(save_dir)
        
        os.remove(os.path.join(save_dir, file_name))