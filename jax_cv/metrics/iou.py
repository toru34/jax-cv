import jax.numpy as jnp

from jax_cv.metrics.utils import _intersection_bbox, _intersection_mask, _intersection_polygons, _union_bbox, _union_mask, _union_polygons


def iou_bbox(bbox1, bbox2):
    intersection = _intersection_bbox(bbox1, bbox2)
    union = _union_bbox(bbox1, bbox2)

    return intersection / union


def iou_mask(mask1, mask2):
    intersection = _intersection_mask(mask1, mask2)
    union = _union_mask(mask1, mask2)

    return intersection / union


def iou_polygons(polygons1, polygons2):
    intersection = _intersection_polygons(polygons1, polygons2)
    union = _union_polygons(polygons1, polygons2)

    return intersection / union