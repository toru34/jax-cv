import jax.numpy as jnp

from jax_cv.metrics.utils import _area_bbox, _area_mask, _area_polygons, _intersection_bbox, _intersection_mask, _intersection_polygons


def dice_bbox(bbox1, bbox2):
    intersection = _intersection_bbox(bbox1, bbox2)

    area_bbox1 = _area_bbox(bbox1)
    area_bbox2 = _area_bbox(bbox2)

    return 2 * intersection / (area_bbox1 + area_bbox2)


def dice_mask(mask1, mask2):
    # TODO: consider if we really need to put assertion here
    assert mask1.shape == mask2.shape

    intersection = _intersection_mask(mask1, mask2)

    area_mask1 = _area_mask(mask1)
    area_mask2 = _area_mask(mask2)

    return 2 * intersection / (area_mask1 + area_mask2)


def dice_polygons(polygons1, polygons2):
    intersection = _intersection_polygons(polygons1, polygons2)

    area_polygons1 = _area_polygons(polygons1)
    area_polygons2 = _area_polygons(polygons2)

    return 2 * intersection / (area_polygons1 + area_polygons2)