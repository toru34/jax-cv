import numpy as np
import jax.numpy as jnp
from PIL import Image, ImageDraw

from jax_cv.utils.utils import _polygons_to_mask


def _area_bbox(bbox):
    return (bbox['xmax'] - bbox['xmin']) * (bbox['ymax'] - bbox['ymin'])


def _area_mask(mask):
    mask = mask.astype('bool')

    return mask.sum()


def _area_polygons(polygons):
    mask = _polygons_to_mask(polygons)

    return _area_mask(mask)


def _intersection_bbox(bbox1, bbox2):
    xmin = max(bbox1['xmin'], bbox2['xmin'])
    xmax = min(bbox1['xmax'], bbox2['xmax'])
    
    ymin = max(bbox1['ymin'], bbox2['ymin'])
    ymax = min(bbox1['ymax'], bbox2['ymax'])
    
    return max(0, max(0, (xmax - xmin)) * max(0, (ymax - ymin)))


def _intersection_mask(mask1, mask2):
    # TODO: consider putting assertion here
    if mask1.shape != mask2.shape:
        mask1_shape = mask1.shape
        mask2_shape = mask2.shape

        shape = np.max([mask1_shape, mask2_shape], axis=0)

        mask1 = np.pad(mask1, ((0, shape[0] - mask1_shape[0]), (0, shape[1] - mask1_shape[1])))
        mask2 = np.pad(mask2, ((0, shape[0] - mask2_shape[0]), (0, shape[1] - mask2_shape[1])))

    mask1 = mask1.astype('bool')
    mask2 = mask2.astype('bool')

    return np.logical_and(mask1, mask2).sum()


def _intersection_polygons(polygons1, polygons2):

    # TODO:
    mask1 = _polygons_to_mask(polygons1)
    mask2 = _polygons_to_mask(polygons2)

    # TODO: explore better way
    # Reshape masks to have same shape
    mask1_shape = mask1.shape
    mask2_shape = mask2.shape

    shape = np.max([mask1_shape, mask2_shape], axis=0)

    mask1 = np.pad(mask1, ((0, shape[0] - mask1_shape[0]), (0, shape[1] - mask1_shape[1])))
    mask2 = np.pad(mask2, ((0, shape[0] - mask2_shape[0]), (0, shape[1] - mask2_shape[1])))

    return _intersection_mask(mask1, mask2)


def _union_bbox(bbox1, bbox2):
    # TODO: change names to make them less confusing
    area_bbox1 = _area_bbox(bbox1)
    area_bbox2 = _area_bbox(bbox2)

    intersection = _intersection_bbox(bbox1, bbox2)

    return area_bbox1 + area_bbox2 - intersection


def _union_mask(mask1, mask2):
    area_mask1 = _area_mask(mask1)
    area_mask2 = _area_mask(mask2)

    intersection = _intersection_mask(mask1, mask2)

    return area_mask1 + area_mask2 - intersection


def _union_polygons(polygons1, polygons2):

    area_polygons1 = _area_polygons(polygons1)
    area_polygons2 = _area_polygons(polygons2)

    intersection = _intersection_polygons(polygons1, polygons2)

    return area_polygons1 + area_polygons2 - intersection