from collections import defaultdict

import numpy as np

from jax_cv.metrics import iou_bbox, iou_polygons


def average_precision(y_true, y_pred, iou_threshold=0.5, criterion='bbox', interpolation_method='coco'):
    """Compute average precision for each category

    Arguments
    ---------
        y_true: list
            aaa
        y_pred: list
            aaa
        iou_threshold: float, optional
            aaa
        criterion: str, optional
            aaa
        interpolation_method: str, optional
            aaa

    Returns
    -------
        average_precision: dict
            aaa
        precision: dict
            aaa
        recall: dict
            aaa
    """
    assert len(y_true) == len(y_pred)

    # TODO: check if precision and recall arange in correct order

    # Calculate the number of ground truths for each of them.
    category2counts = defaultdict(lambda: 0)

    for instances in y_true:
        for instance in instances:
            category2counts[instance['category']] += 1
    
    for instances in y_pred:
        for instance in instances:
            category2counts[instance['category']] += 0
    
    # Compute average precision for each category
    average_precision, precision, recall = dict(), dict(), dict()

    for category, n_ground_truths in category2counts.items():
    
        # If there is no ground truth, recall can't be computed, so it'll be set as none.
        if n_ground_truths == 0:
            average_precision[category] = None
            continue

        # Match predicted instance to ground truth based on IoU score between the two.
        instance_metrics = []

        for _instances_true, _instances_pred in zip(y_true, y_pred):
            instances_true = [instance for instance in _instances_true if instance['category'] == category]
            instances_pred = [instance for instance in _instances_pred if instance['category'] == category]

            # For each predicted instance, compute IoU towards nearest ground truth.
            for i in range(len(instances_pred)):
                highest_iou = 0.0
                nearest_ground_truth = - 1

                for j in range(len(instances_true)):
                    if criterion == 'bbox':
                        iou = iou_bbox(instances_true[j]['bbox'], instances_pred[i]['bbox'])
                    elif criterion == 'mask':
                        raise NotImplementedError

                    if iou > highest_iou:
                        highest_iou = iou
                        nearest_ground_truth = j
            
                instances_pred[i]['iou'] = highest_iou
                instances_pred[i]['nearest_ground_truth'] = nearest_ground_truth

                if highest_iou > iou_threshold:
                    instances_pred[i]['tp'] = True
                else:
                    instances_pred[i]['tp'] = False
            
            # Only the predicted instance with the highest IoU is considered as true positive, and the others are considered as false positive.
            for i in range(len(instances_pred)):
                for j in range(len(instances_pred)):
                    if i == j or instances_pred[i]['nearest_ground_truth'] != instances_pred[j]['nearest_ground_truth']:
                        continue
                    
                    if instances_pred[i]['iou'] < instances_pred[j]['iou']:
                        instances_pred[i]['tp'] = False
            
            # Collect information needed to compute precision and recall
            instance_metrics += [{
                'tp': instance['tp'],
                'confidence': instance['confidence']
            } for instance in instances_pred]
        
        # Sort instance metrics in confidence score (descending order).
        instance_metrics = sorted(instance_metrics, key=lambda x: - x['confidence'])

        # Compute precision and recall
        n_true_positives, n_false_positives = 0, 0

        for i in range(len(instance_metrics)):
            if instance_metrics[i]['tp']:
                n_true_positives += 1
            else:
                n_false_positives += 1
            
            instance_metrics[i]['precision'] = n_true_positives / (n_true_positives + n_false_positives)
            instance_metrics[i]['recall'] = n_true_positives / n_ground_truths
    
        # Sort instance metrics in confidence score (ascending order)
        # Do not do sorting here because it collapses the original order. (?)
        # TODO:
        instance_metrics = instance_metrics[::-1]

        # Collect precision and recall
        _precision = [instance['precision'] for instance in instance_metrics]
        _recall = [instance['recall'] for instance in instance_metrics]

        # Compute area under precision-recall curve, which is average precision.
        auc = _area_under_curve(_precision, _recall)

        average_precision[category] = auc
        precision[category] = _precision
        recall[category] = _recall

    return average_precision, precision, recall

def _area_under_curve(precision, recall):
    """Compute area under precision-recall curve

    Arguments
    ---------
        precision: list
            aaa
        recall: list
            aaa
    
    Returns
    -------
        auc: float
            aaa
    """
    # TODO: check if precision and recall arange in correct order
    assert len(precision) == len(recall)

    # Add dummy values to make computation easy
    precision.append(0.0)
    recall.append(0.0)

    # Interpolate precision curve
    precision_interpolated = [np.max(precision[:i+1]) for i in range(len(precision))]

    # Compute area under curve
    auc = np.sum([
        (recall[i] - recall[i+1]) * precision_interpolated[i]
    for i in range(len(precision_interpolated) - 1)])
    
    return auc