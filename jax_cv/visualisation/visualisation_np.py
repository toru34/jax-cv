import numpy as np
import skimage.draw


def draw_bbox_np(
        image,
        bbox,
        color,
        category,
        linewidth=3
    ):
    """Draw bounding box on numpy array.

    Args:
        image (numpy.ndarray):
        bbox (dict):
        color (str):
        category (str):
        linewidth (int):
    """

    interval = 10 # TODO

    height, width = image.shape[:2]

    xmin = bbox['xmin']
    ymin = bbox['ymin']
    xmax = bbox['xmax']
    ymax = bbox['ymax']

    x = np.arange(width)
    y = np.arange(height)

    for i in range(-(linewidth - 1) // 2, (linewidth + 1) // 2):
        line_x = np.concatenate([x[xmin+j:xmax+1:interval*2] for j in range(interval)])
        line_y = np.concatenate([y[ymin+j:ymax+1:interval*2] for j in range(interval)])

        image[max(0, ymin+i), line_x] = 255 # TODO
        image[min(height-1, ymax+i), line_x] = 255 # TODO
        image[line_y, max(0, xmin+i)] = 255 # TODO
        image[line_y, min(width-1, xmax+i)] = 255 # TODO
    

def draw_mask_np(
        image,
        polygon,
        color,
        linewidth=3
    ):
    """Draw (segmentation) mask on numpy array.

    Args:
        image (numpy.ndarray):
        polygon (numpy.ndarray):
        color (str):
        linewidth (int):
    """
    x, y = polygon.T.astype('int32')
    _contour_y, _contour_x = skimage.draw.polygon_perimeter(y, x, shape=image.shape)

    height, width = image.shape[:2]

    contour_y = []
    contour_x = []
    
    for i in range(-(linewidth - 1) // 2, (linewidth + 1) // 2):
        c_y = np.clip(_contour_y + i, 0, height - 1)
        c_x = np.clip(_contour_x, 0, width - 1)

        contour_y.append(c_y)
        contour_x.append(c_x)

        c_y = np.clip(_contour_y + i, 0, height - 1)
        c_x = np.clip(_contour_x, 0, width - 1)

        contour_y.append(c_y)
        contour_x.append(c_x)
    
    contour_y = np.concatenate(contour_y, axis=0)
    contour_x = np.concatenate(contour_x, axis=0)

    mask_y, mask_x = skimage.draw.polygon(y, x, shape=image.shape) # TODO: add alpha

    image[contour_y, contour_x] = 255 # TODO:
    image[mask_y, mask_x] = 255 # TODO: