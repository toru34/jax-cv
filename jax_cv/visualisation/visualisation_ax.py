from matplotlib import patches


def draw_bbox_ax(
        ax,
        bbox,
        color,
        category
    ):
    """Draw bounding box on matplotlib axes.

    Args:
        ax (matplotlib.axes):
        bbox (dict):
        color (str):
        category (str):
    """

    linewidth = 2 # TODO
    alpha = 0.7

    bbox_patch = patches.Rectangle((bbox['xmin'], bbox['ymin']), bbox['xmax'] - bbox['xmin'], bbox['ymax'] - bbox['ymin'], linewidth=linewidth, alpha=alpha, linestyle='dashed', edgecolor=color, facecolor='none')

    ax.add_patch(bbox_patch)
    ax.text(bbox['xmin'], bbox['ymin'] - 8, category, color=color, size=10, backgroundcolor='none')


def draw_mask_ax(
        ax,
        polygon,
        color
    ):
    """Draw (segmentation) mask on matplotlib axes.

    Args:
        ax (matplotlib.axes):
        polygon (numpy.ndarray):
        color (str):
    """
    contour_patch = patches.Polygon(polygon, facecolor='none', edgecolor=color)
    mask_patch = patches.Polygon(polygon, facecolor=color, alpha=0.5)

    ax.add_patch(contour_patch)
    ax.add_patch(mask_patch)