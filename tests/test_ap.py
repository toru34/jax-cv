import unittest

from jax_cv.metrics.average_precision import _area_under_curve

precision = [0.2916, 0.3043, 0.2727, 0.2857, 0.3, 0.3157, 0.3333, 0.3529, 0.375, 0.4, 0.4285, 0.3846, 0.3333, 0.2727, 0.3, 0.2222, 0.25, 0.2857, 0.3333, 0.4, 0.5, 0.6666, 0.5, 1]
recall = [0.4666, 0.4666, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.3333, 0.2666, 0.2, 0.2, 0.1333, 0.1333, 0.1333, 0.1333, 0.1333, 0.1333, 0.1333, 0.0666, 0.0666]

class TestAP(unittest.TestCase):
    def test_ap(self):
        self.assertEqual(_area_under_curve(precision, recall), 0.24560955, f"Should be {0.24560955}")

if __name__ == '__main__':
    unittest.main()