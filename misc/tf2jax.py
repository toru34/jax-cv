weights = {}
axes = (3, 2, 0, 1)

with h5py.File('/Users/toru34/.jax_nn/pretrained_weights/vgg19_weights_tf_dim_ordering_tf_kernels.h5', 'r') as f:
    weights['block1_conv1'] = {
        'W': f['block1_conv1']['block1_conv1_W_1:0'][()].transpose(axes),
        'b': f['block1_conv1']['block1_conv1_b_1:0'][()]
    }
    weights['block1_conv2'] = {
        'W': f['block1_conv2']['block1_conv2_W_1:0'][()].transpose(axes),
        'b': f['block1_conv2']['block1_conv2_b_1:0'][()]
    }
    weights['block2_conv1'] = {
        'W': f['block2_conv1']['block2_conv1_W_1:0'][()].transpose(axes),
        'b': f['block2_conv1']['block2_conv1_b_1:0'][()]
    }
    weights['block2_conv2'] = {
        'W': f['block2_conv2']['block2_conv2_W_1:0'][()].transpose(axes),
        'b': f['block2_conv2']['block2_conv2_b_1:0'][()]
    }
    weights['block3_conv1'] = {
        'W': f['block3_conv1']['block3_conv1_W_1:0'][()].transpose(axes),
        'b': f['block3_conv1']['block3_conv1_b_1:0'][()]
    }
    weights['block3_conv2'] = {
        'W': f['block3_conv2']['block3_conv2_W_1:0'][()].transpose(axes),
        'b': f['block3_conv2']['block3_conv2_b_1:0'][()]
    }
    weights['block3_conv3'] = {
        'W': f['block3_conv3']['block3_conv3_W_1:0'][()].transpose(axes),
        'b': f['block3_conv3']['block3_conv3_b_1:0'][()]
    }
    weights['block3_conv4'] = {
        'W': f['block3_conv4']['block3_conv4_W_1:0'][()].transpose(axes),
        'b': f['block3_conv4']['block3_conv4_b_1:0'][()]
    }
    weights['block4_conv1'] = {
        'W': f['block4_conv1']['block4_conv1_W_1:0'][()].transpose(axes),
        'b': f['block4_conv1']['block4_conv1_b_1:0'][()]
    }
    weights['block4_conv2'] = {
        'W': f['block4_conv2']['block4_conv2_W_1:0'][()].transpose(axes),
        'b': f['block4_conv2']['block4_conv2_b_1:0'][()]
    }
    weights['block4_conv3'] = {
        'W': f['block4_conv3']['block4_conv3_W_1:0'][()].transpose(axes),
        'b': f['block4_conv3']['block4_conv3_b_1:0'][()]
    }
    weights['block4_conv4'] = {
        'W': f['block4_conv4']['block4_conv4_W_1:0'][()].transpose(axes),
        'b': f['block4_conv4']['block4_conv4_b_1:0'][()]
    }
    weights['block5_conv1'] = {
        'W': f['block5_conv1']['block5_conv1_W_1:0'][()].transpose(axes),
        'b': f['block5_conv1']['block5_conv1_b_1:0'][()]
    }
    weights['block5_conv2'] = {
        'W': f['block5_conv2']['block5_conv2_W_1:0'][()].transpose(axes),
        'b': f['block5_conv2']['block5_conv2_b_1:0'][()]
    }
    weights['block5_conv3'] = {
        'W': f['block5_conv3']['block5_conv3_W_1:0'][()].transpose(axes),
        'b': f['block5_conv3']['block5_conv3_b_1:0'][()]
    }
    weights['block5_conv4'] = {
        'W': f['block5_conv4']['block5_conv4_W_1:0'][()].transpose(axes),
        'b': f['block5_conv4']['block5_conv4_b_1:0'][()]
    }
    weights['block6_dense1'] = {
        'W': f['fc1']['fc1_W_1:0'][()],
        'b': f['fc1']['fc1_b_1:0'][()]
    }
    weights['block6_dense2'] = {
        'W': f['fc2']['fc2_W_1:0'][()],
        'b': f['fc2']['fc2_b_1:0'][()]
    }
    weights['block6_dense3'] = {
        'W': f['predictions']['predictions_W_1:0'][()],
        'b': f['predictions']['predictions_b_1:0'][()]
    }

with open('/Users/toru34/.jax_nn/pretrained_weights/vgg19_weights.pickle', 'wb') as f:
    pickle.dump(weights, f)