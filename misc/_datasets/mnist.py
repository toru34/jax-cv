from .utils import * # TODO: replace * with functions

def load_mnist(onehot=False):
    n_classes = 10
    base_url = 'http://yann.lecun.com/exdb/mnist/'
    dataset_dir = os.path.join(base_dir, 'mnist')

    train_image_file = 'train-images-idx3-ubyte.gz'
    train_label_file = 'train-labels-idx1-ubyte.gz'
    test_image_file = 't10k-images-idx3-ubyte.gz'
    test_label_file = 't10k-labels-idx1-ubyte.gz'

    # Create directory if it doesn't exist
    if not os.path.exists(dataset_dir):
        os.makedirs(dataset_dir)
    
    # Download dataset if it doesn't exist
    for file_name in [train_image_file, train_label_file, test_image_file, test_label_file]:
        if not os.path.isfile(os.path.join(dataset_dir, file_name)):
            _download_file(base_url, file_name, dataset_dir)
    
    # Read dataset
    train_images = _parse_mnist_image_file(os.path.join(dataset_dir, train_image_file))
    train_labels = _parse_mnist_label_file(os.path.join(dataset_dir, train_label_file))
    test_images = _parse_mnist_image_file(os.path.join(dataset_dir, test_image_file))
    test_labels = _parse_mnist_label_file(os.path.join(dataset_dir, test_label_file))

    # Convert labels into onehot representations if it's necessary
    if onehot:
        train_labels = np.eye(n_classes)[train_labels]
        test_labels = np.eye(n_classes)[test_labels]

    return (train_images, train_labels), (test_images, test_labels)