def load_cifar100(onehot=False):
    n_classes = 100
    base_url = 'https://www.cs.toronto.edu/~kriz/'
    dataset_dir = os.path.join(base_dir, 'cifar100')

    file_name = 'cifar-100-python.tar.gz'

    # Create directory if it doesn't exist
    if not os.path.exists(dataset_dir):
        os.makedirs(dataset_dir)

    # Download dataset if it doesn't exist
    if not os.path.isfile(os.path.join(dataset_dir, file_name)):
        _download_file(base_url, file_name, dataset_dir)

    # Unpack file
    if not os.path.exists(os.path.join(dataset_dir, 'cifar-100-python')):
        with tarfile.open(os.path.join(dataset_dir, file_name), 'r') as f:
            f.extractall(dataset_dir)
    
    # Read dataset
    with open(os.path.join(dataset_dir, 'cifar-100-python', 'train'), 'rb') as f:
        train_data = pickle.load(f, encoding='bytes')

    train_images = np.array(train_data[b'data'].reshape(-1, 3, 32, 32))
    train_labels = np.array(train_data[b'fine_labels'])
    
    with open(os.path.join(dataset_dir, 'cifar-100-python', 'test'), 'rb') as f:
        test_data = pickle.load(f, encoding='bytes')

    test_images = np.array(test_data[b'data'].reshape(-1, 3, 32, 32))
    test_labels = np.array(test_data[b'fine_labels'])

    if onehot:
        train_labels = np.eye(n_classes)[train_labels]
        test_labels = np.eye(n_classes)[test_labels]
    
    return (train_images, train_labels), (test_images, test_labels)
