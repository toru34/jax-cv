import os
import gzip
import array
import requests
from tqdm import tqdm

import jax.numpy as np

# Adapted from https://github.com/google/jax/blob/master/examples/datasets.py under MIT LICENSE
def _parse_mnist_image_file(file_path):
    with gzip.open(file_path, 'rb') as f:
        _, n_data, n_rows, n_cols = struct.unpack('>IIII', f.read(16))
        return np.array(array.array('B', f.read()), dtype=np.uint8).reshape(n_data, n_rows, n_cols)

# Adapted from https://github.com/google/jax/blob/master/examples/datasets.py under MIT LICENSE
def _parse_mnist_label_file(file_path):
    with gzip.open(file_path, 'rb') as f:
        struct.unpack('>II', f.read(8))
        return np.array(array.array('B', f.read()), dtype=np.uint8)