def load_cifar10(onehot=False):
    def _read_file(file_path):
        with open(file_path, 'rb') as f:
            data = pickle.load(f, encoding='bytes')
        return data

    n_classes = 10
    base_url = 'https://www.cs.toronto.edu/~kriz/'
    dataset_dir = os.path.join(base_dir, 'cifar10')

    file_name = 'cifar-10-python.tar.gz'

    # Create directory if it doesn't exist
    if not os.path.exists(dataset_dir):
        os.makedirs(dataset_dir)
    
    # Download dataset if it doesn't exist
    if not os.path.isfile(os.path.join(dataset_dir, file_name)):
        _download_file(base_url, file_name, dataset_dir)

    # Unpack file
    if not os.path.exists(os.path.join(dataset_dir, 'cifar-10-batches-py')):
        with tarfile.open(os.path.join(dataset_dir, file_name), 'r') as f:
            f.extractall(dataset_dir)
    
    # Read dataset
    train_files = [os.path.join(dataset_dir, 'cifar-10-batches-py', f'data_batch_{i+1}') for i in range(5)]
    test_file = os.path.join(dataset_dir, 'cifar-10-batches-py', 'test_batch')

    train_images = []
    train_labels = []
    for train_file in train_files:
        data = _read_file(train_file)

        train_images.append(data[b'data'].reshape(-1, 3, 32, 32))
        train_labels.append(np.array(data[b'labels']))
    
    train_images = np.concatenate(train_images, axis=0)
    train_labels = np.concatenate(train_labels, axis=0)

    data = _read_file(test_file)

    test_images = np.array(data[b'data'].reshape(-1, 3, 32, 32))
    test_labels = np.array(data[b'labels'])

    if onehot:
        train_labels = np.eye(10)[train_labels]
        test_labels = np.eye(10)[test_labels]
    
    return (train_images, train_labels), (test_images, test_labels)