def load_camvid():
    def _read_camvid_dataset(path):
        with open(path, 'r') as f:
            image_files = []
            label_files = []
            for line in f.readlines():
                image_path, label_path = line.strip().split(' ')
                image_file = image_path.split('/')[-1]
                label_file = label_path.split('/')[-1]

                image_files.append(image_file)
                label_files.append(label_file)
        
        return image_files, label_files

    base_url = 'https://github.com/alexgkendall/SegNet-Tutorial/archive/'
    dataset_dir = os.path.join(base_dir, 'camvid')

    file_name = 'master.zip'

    # Create directory if it doesn't exist
    if not os.path.exists(dataset_dir):
        os.makedirs(dataset_dir)

    # Download dataset if it doesn't exist
    if not os.path.isfile(os.path.join(dataset_dir, file_name)):
        _download_file(base_url, file_name, dataset_dir)
    
    # Unpack file
    if not os.path.exists(os.path.join(dataset_dir, 'SegNet-Tutorial-master')):
        with zipfile.ZipFile(os.path.join(dataset_dir, file_name), 'r') as f:
            f.extractall(dataset_dir)
    
    # Read files
    dataset = {}
    for dataset_type in ['train', 'valid', 'test']:
        _dataset_type = dataset_type if dataset_type in ('train', 'test') else 'val'

        image_files, label_files = _read_camvid_dataset(os.path.join(dataset_dir, 'SegNet-Tutorial-master/CamVid', f'{_dataset_type}.txt'))

        dataset[dataset_type] = {
            'image_dir': os.path.join(dataset_dir, 'SegNet-Tutorial-master/CamVid', _dataset_type),
            'image_files': image_files,
            'label_dir': os.path.join(dataset_dir, 'SegNet-Tutorial-master/CamVid', f'{_dataset_type}annot'),
            'label_files': label_files
        }

    return dataset