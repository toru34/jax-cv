import os
import requests
from tqdm import tqdm

from .utils import _download_file

base_dir = os.path.join(os.path.expanduser('~'), '.jax_nlp/datasets/')

def load_ptb():
    base_url = 'https://raw.githubusercontent.com/wojzaremba/lstm/master/data/'
    dataset_dir = os.path.join(base_dir, 'ptb')

    train_file = 'ptb.train.txt'
    valid_file = 'ptb.valid.txt'
    test_file = 'ptb.test.txt'

    # Create directory if it doesn't exist
    if not os.path.exists(dataset_dir):
        os.makedirs(dataset_dir)

    # Download dataset if it doesn't exist
    for file_name in [train_file, valid_file, test_file]:
        if not os.path.isfile(os.path.join(dataset_dir, file_name)):
            _download_file(base_url, file_name, dataset_dir)