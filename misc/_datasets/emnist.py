def load_emnist(onehot=False):
    base_url = 'http://www.itl.nist.gov/iaui/vip/cs_links/EMNIST/'
    dataset_dir = os.path.join(base_dir, 'emnist')

    file_name = 'gzip.zip'

    # Create directory if it doesn't exist
    if not os.path.exists(dataset_dir):
        os.makedirs(dataset_dir)

    # Download dataset if it doesn't exist
    if not os.path.isfile(os.path.join(dataset_dir, file_name)):
        _download_file(base_url, file_name, dataset_dir)
    
    # Unpack file
    if not os.path.exists(os.path.join(dataset_dir, 'gzip')):
        with zipfile.ZipFile(os.path.join(dataset_dir, file_name), 'r') as f:
            f.extractall(dataset_dir)