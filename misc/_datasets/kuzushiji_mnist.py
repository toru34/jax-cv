def load_kuzushiji_mnist(onehot=False):
    n_classes = 10
    base_url = 'http://codh.rois.ac.jp/kmnist/dataset/kmnist/'
    dataset_dir = os.path.join(base_dir, 'kuzushiji_mnist')

    train_image_file = 'kmnist-train-imgs.npz'
    train_label_file = 'kmnist-train-labels.npz'
    test_image_file = 'kmnist-test-imgs.npz'
    test_label_file = 'kmnist-test-labels.npz'
    classname_file = 'kmnist_classmap.csv'

    # Create directory if it doesn't exist
    if not os.path.exists(dataset_dir):
        os.makedirs(dataset_dir)

    # Download dataset if it doesn't exist
    for file_name in [train_image_file, train_label_file, test_image_file, test_label_file, classname_file]:
        if not os.path.isfile(os.path.join(dataset_dir, file_name)):
            _download_file(base_url, file_name, dataset_dir)
    
    # Read dataset
    train_images = np.load(os.path.join(dataset_dir, train_image_file))['arr_0']
    train_labels = np.load(os.path.join(dataset_dir, train_label_file))['arr_0']
    test_images = np.load(os.path.join(dataset_dir, test_image_file))['arr_0']
    test_labels = np.load(os.path.join(dataset_dir, test_label_file))['arr_0']

    with open(os.path.join(dataset_dir, classname_file), 'r') as f:
        csv_reader = csv.DictReader(f, delimiter=',')
        id2name_dict = {row['index']: row['char'] for row in csv_reader}

    # Convert labels into onehot representations if it's necessary
    if onehot:
        train_labels = np.eye(n_classes)[train_labels]
        test_labels = np.eye(n_classes)[test_labels]

    return (train_images, train_labels), (test_images, test_labels), id2name_dict