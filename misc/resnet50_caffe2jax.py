# Docker command
# docker run -it -v /Users/toru34:/Users/toru34 ashokponkumar/caffe-py3:latest

import re
import pickle
from collections import OrderedDict

import caffe
import numpy as np

model_path = '/Users/toru34/.jax_cv/pretrained_weights/ResNet-50-model.caffemodel'
config_path = '/Users/toru34/.jax_cv/pretrained_weights/ResNet-50-deploy.prototxt'

caffe.set_mode_cpu();

model = caffe.Net(config_path, model_path, caffe.TEST)

c2i = {
    'a': '1',
    'b': '2',
    'c': '3',
    'd': '4',
    'e': '5',
    'f': '6'
}

weights = OrderedDict({})

for key, value in model.params.items():
    match1 = re.match(r'([a-z]+)(\d+)([a-z])_branch(\d+)([a-z])', key)
    match2 = re.match(r'([a-z]+)(\d+)([a-z])', key)

    if match1:
        _module, _block, _subblock, _, _layer = match1.groups()

        block = 'block' + _block
        subblock = 'resblock' + c2i[_subblock]
        layer = c2i[_layer]

    elif match2:
        _module, _block, _subblock = match2.groups()

        block = 'block' + _block
        subblock = 'resblock' + c2i[_subblock]
        layer = '0'
    else:
        # print(key)
        # continue
        if key == 'conv1':
            block = 'block1'
            module = 'conv1'

            if block not in weights:
                weights[block] = OrderedDict({})

            if module not in weights[block]:
                weights[block][module] = OrderedDict({})
            
            weights[block][module]['W'] = model.params[key][0].data
            weights[block][module]['b'] = model.params[key][1].data
            
        elif key == 'bn_conv1':
            block = 'block1'
            module = 'batch_norm1'

            if block not in weights:
                weights[block] = OrderedDict({})

            if module not in weights[block]:
                weights[block][module] = OrderedDict({})
            
            weights[block][module]['population_stats'] = OrderedDict({
                'mean': model.params[key][0].data,
                'std': model.params[key][1].data
            })

        elif key == 'scale_conv1':
            block = 'block1'
            module = 'batch_norm1'

            if block not in weights:
                weights[block] = OrderedDict({})

            if module not in weights[block]:
                weights[block][module] = OrderedDict({})
            
            weights[block][module]['gamma'] = model.params[key][0].data
            weights[block][module]['beta'] = model.params[key][1].data
        
        elif key == 'fc1000':
            block = 'block6'
            module = 'dense'

            if block not in weights:
                weights[block] = OrderedDict({})

            if module not in weights[block]:
                weights[block][module] = OrderedDict({})
            
            weights[block][module]['W'] = model.params[key][0].data.T
            weights[block][module]['b'] = model.params[key][1].data

        else:
            print(key)
        
        continue

    if block not in weights:
        weights[block] = OrderedDict({})
    
    if subblock not in weights[block]:
        weights[block][subblock] = OrderedDict({})

    if _module == 'res':
        module = 'conv' + layer

        if module not in weights[block][subblock]:
            weights[block][subblock][module] = OrderedDict({})
        
        weights[block][subblock][module]['W'] = model.params[key][0].data
    
    elif _module == 'bn':
        module = 'batch_norm' + layer

        if module not in weights[block][subblock]:
            weights[block][subblock][module] = OrderedDict({})

        weights[block][subblock][module]['population_stats'] = OrderedDict({
            'mean': model.params[key][0].data,
            'std': model.params[key][1].data
        })
    
    elif _module == 'scale':
        module = 'batch_norm' + layer

        if module not in weights[block][subblock]:
            weights[block][subblock][module] = OrderedDict({})
        
        weights[block][subblock][module]['gamma'] = model.params[key][0].data
        weights[block][subblock][module]['beta'] = model.params[key][1].data

with open('resnet50_imagenet.pickle', 'wb') as f:
    pickle.dump(weights, f)

np.save('resnet50_imagenet', weights)